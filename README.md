# Coding Test

Test environment:
* Ubuntu 19.10
* GCC 9.2.1
* Clang 9.0.0-2
* CPU: AMD® Ryzen 7 2700

## Initial thoughts

**Note:** *All results are average from 10 runs.*

#### GCC performance with -O0:
* Init time: 1980.27 ms
* Average render time: 2808.69 ms

#### GCC performance with -03:
* Init time: 636.761 ms
* Average render time: 546.288 ms

#### Clang performance with -O0:
* Init time: 4868.59 ms
* Average render time: 2697.2 ms

#### Clang performance with -03:
* Init time: 2460.38 ms
* Average render time: 583.967 ms

Initially we can observe, that this code favors GCC compiler in terms of memory allocation.

```
movl    $48, %edi
callq   operator new(unsigned long)
movq    %rax, %r13
```
```
movl    $48, %edi
call    operator new(unsigned long)
movq    %rax, 40(%rsp)
```

We can observe that assembly for memory allocation is the same on both compilers so the difference must be in some other place.

Memory usage is consistent between O0 and 03 builds, so no padding is added.

*From this point all tests will be performed with GCC compiler.*

## Profiling results

Results from Valgrind Memcheck:
```
HEAP SUMMARY:
in use at exit: 320,000,480 bytes in 10,000,010 blocks
total heap usage: 10,000,070 allocs, 60 frees, 588,510,448 bytes allocated
```

Results from Valgrind Callcheck:
<img src="Screenshot from 2020-01-15 20-35-16.png" />

## Process of optimizing

* Preallocation of memory
> Preallocation of memory for vectors improved init time to 1612.21(372.417)* (~23%(71%)* faster).
```C++
    for(auto& arrObject : arrObjects)
		{
			arrObject = static_cast<CObject*>(malloc(sizeof(CObject)));
		}
    [...]
    new (rWorldState.arrObjects[i]) CObject(vPosition, i);
```
> Allocation of memory in one single block did not improve init performance.
```c++
    char* objects = (char*)(malloc(sizeof(CObject) * OBJECT_COUNT));
    for(int i = 0; i < OBJECT_COUNT; i++)
      arrObjects[i] = (CObject*)(&objects[0] + sizeof(CObject) * i);
    [...]
    new (rWorldState.arrObjects[i]) CObject(vPosition, i);
```
> Main thing that is slowing Init funtion is generating pseudorandom numbers:
```
const Vec3 vPosition = Vec3(dist(rWorldState.rnd_gen), dist(rWorldState.rnd_gen), dist(rWorldState.rnd_gen));
```
> In my opinion, the best way to optimize Init function would be to generate values at compile time.
> I believe that there are two approaches to do this:
> * marking Render funtion as constexpr: because of the fact that std::uniform_real_distribution is of non literal type, I was not able to do that. I could implement my own version of uniform_real_distribution that is costexpr and produces the same result, but it was outside of the time frame for this test.
> * generating numbers upfront: generated file was 400mb. Linking failed because of not enough RAM.

* Processing data with multiple threads
> My current implementation relies on splitting objects vector by the number of threads.
```C++
std::array<std::array<CObject*, OBJECT_COUNT / THREADS_COUNT>*, THREADS_COUNT> array;
[...]
for(int j = 0; j < THREADS_COUNT; j++)
		{
			array[j] = new std::array<CObject*, OBJECT_COUNT / THREADS_COUNT>();
			for(int i = 0; i < OBJECT_COUNT / THREADS_COUNT; i++)
				array.at(j)->at(i) = (CObject*)(malloc(sizeof(CObject)));
		}
[...]
threads.clear();
		for (const auto& arr : rWorldState.array)
		{
			auto fun = [&]()
			{
				for(const auto& a : *arr)
				if (pObserver->GetBounds().Overlaps(a->GetPosition()))
				{
					m.lock();
					rOverlappingObjects.push_back(a->GetIdentifier());
					m.unlock();
				}
			};
			threads.emplace_back(std::thread(fun));
		}
		for(auto& thread : threads)
			thread.join();
```
> *The same process could be applied to the memory allocation.*
> *However, this implementation did not improve performance, probably because of false sharing.*
>
> I believe proceeding this way avoids many cache misses as opposed to executing per observer.
> Changes in Render function gave us 21% quicker execution on single thread.
> Results are as present:

| Threads | Init time | Render time | Init / Render difference (rel. 1 thr.) [%] |
|:-------:|:---------:|:-----------:|:----------------------------:|
|1|1703 (383.69)*|2318.05 (685.455)* | 0 (0)* / 0 (0)* |
|2|1666.7 (411.707)*|1207.38 (385.155)*| \~1 (\~-7)* / \~92 (\~78)* |
|4|1664.67 (417.659)*|688.03 (236.906)*| \~2 (\~-8)* / \~237 (\~189)* |
|8|1654.99 (409.219)*|454.587 (155.324)*| \~3 (\~-6)* / \~410 (\~341)* |
|16|1650.91 (408.523)*|321.434 (151.458)*| \~3 (\~-6)* / \~621 (\~353)* |

> We can observe that the best scaling is received when using two cores. It's probably related to the mutex, which is locking ```rOverlappingObjects```.
>
> After all mutex usage removal I did not observe any difference in performance.
```C++
std::array<std::vector<uint64>*, THREADS_COUNT> overlappingObjects;
for(int i = 0; i < THREADS_COUNT; i++)
{
    overlappingObjects[i] = new std::vector<uint64>();
}
[...]
auto& overObjects = overlappingObjects[i];
[...]
overObjects->push_back(a->GetIdentifier());
[..]
```
> The fact that we did not observe any difference is probably related to cache line invalidation. We should ensure that any of the data that we are accessing from different threads do not lay on the same cache line.

\* Values in parenthesis show results with O3 optimizing enabled.
