// Mini Game Engine
//
// Init() will fill the virtual world with 10.000.000 objects (represented by a point and an id 0:N)
// After Init() it is okay to assume that the objects won't move anymore.
//
// Update() will update 10 observers, each represented as an AABB
//
// Render() will use those 10 observers AABBs to cull the existing objects, and output the count of objects overlapping the observers AABBs 

#include <cstdint>
#include <cstdlib>
#include <vector>
#include <random>
#include <iostream>
#include <chrono>

///////////////////////////////////////////////////////////////////////////////
// Common types for fixed size over all platforms
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef float  f32;
typedef double f64;

///////////////////////////////////////////////////////////////////////////////
// constants used throughout the program
enum : uint64 { OBJECT_COUNT   = 10000000 };
enum : uint64 { OBSERVER_COUNT = 10 };
enum : uint32 { SEED           = 12345 }; // hard-coded seed for deterministic random values

///////////////////////////////////////////////////////////////////////////////
// class to represent a 64 bit Vector
class Vec3
{
public:
	explicit Vec3(f64 x) : m_x(x), m_y(x), m_z(x) {}
	Vec3(f64 x, f64 y, f64 z) : m_x(x), m_y(y), m_z(z) {}

	Vec3 operator+(const Vec3 &rOther) const { return Vec3(m_x + rOther.m_x, m_y + rOther.m_y, m_z + rOther.m_z); }
	Vec3 operator-(const Vec3 &rOther) const { return Vec3(m_x - rOther.m_x, m_y - rOther.m_y, m_z - rOther.m_z); }

	f64 x() const { return m_x; }
	f64 y() const { return m_y; }
	f64 z() const { return m_z; }

private:
	f64 m_x, m_y, m_z;
};

///////////////////////////////////////////////////////////////////////////////
// class to represent an Axis-Aligned-Bounding-Box (AABB)
class AABB
{
public:
	AABB() : m_vMin(0), m_vMax(0) {}
	AABB(const Vec3 &rMin, const Vec3 &rMax) : m_vMin(rMin) , m_vMax(rMax) {}

	bool Overlaps(const Vec3 &rPoint) const
	{
		if(rPoint.x() < m_vMin.x()) return false;
		if(rPoint.y() < m_vMin.y()) return false;
		if(rPoint.z() < m_vMin.z()) return false;
		if(rPoint.x() > m_vMax.x()) return false;
		if(rPoint.y() > m_vMax.y()) return false;
		if(rPoint.z() > m_vMax.z()) return false;

		return true;
	}
private:
	Vec3 m_vMin;
	Vec3 m_vMax;
};

///////////////////////////////////////////////////////////////////////////////
// class to represent an object in the virtual world
class CObject
{
public:
	CObject(const Vec3 &rPosition, uint64 nIdentifier) : m_vPosition(rPosition), m_nIdentifier(nIdentifier) {}

	Vec3 GetPosition() const { return m_vPosition; }
	uint64 GetIdentifier() const { return m_nIdentifier; }

private:
	Vec3   m_vPosition;
	uint64 m_nIdentifier;
};

///////////////////////////////////////////////////////////////////////////////
// class to represent an observer into the virtual world
class CObserver
{
public:
	CObserver() : m_Bounds() {}

	void UpdateAABB(const AABB &rBounds) { m_Bounds = rBounds; }

	AABB GetBounds() const { return m_Bounds; }
private:
	AABB m_Bounds;
};

///////////////////////////////////////////////////////////////////////////////
struct WorldState
{
	~WorldState()
	{
		for(auto& object : arrObjects)
			delete object;
		for(auto& observer : arrObservers)
			delete observer;
	}
	std::mt19937_64         rnd_gen;
	std::vector<CObject*>   arrObjects;
	std::vector<CObserver*> arrObservers;
};

///////////////////////////////////////////////////////////////////////////////
f64 Init(WorldState &rWorldState, uint32 nSeed)
{
	auto start = std::chrono::high_resolution_clock::now();

	rWorldState.rnd_gen.seed(nSeed);
	std::uniform_real_distribution<f64> dist(-1e10, +1e10);	
	
	for (uint64 i = 0; i < OBJECT_COUNT; ++i)
	{
		const Vec3 vPosition = Vec3(dist(rWorldState.rnd_gen), dist(rWorldState.rnd_gen), dist(rWorldState.rnd_gen));

		rWorldState.arrObjects.push_back(new CObject(vPosition, i));
	}

	for (uint64 i = 0; i < OBSERVER_COUNT; ++i)
	{
		rWorldState.arrObservers.push_back(new CObserver());
	}
	auto time = std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - start).count();
	std::cout << "Init took " << time << " ms" << std::endl;
	return time;
}

///////////////////////////////////////////////////////////////////////////////
f64 Update(WorldState &rWorldState)
{
	auto start = std::chrono::high_resolution_clock::now();

	std::uniform_real_distribution<f64> dist_center(-1e10, +1e10);	
	std::uniform_real_distribution<f64> dist_extends(+20000000, +40000000);	

	for (CObserver *pObserver : rWorldState.arrObservers)
	{
		const Vec3 vCenter = Vec3(dist_center(rWorldState.rnd_gen), dist_center(rWorldState.rnd_gen), dist_center(rWorldState.rnd_gen));
		const f64 fExtends = dist_extends(rWorldState.rnd_gen);

		pObserver->UpdateAABB(AABB(vCenter - Vec3(fExtends), vCenter + Vec3(fExtends)));
	}

	return std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - start).count();
}

///////////////////////////////////////////////////////////////////////////////
f64 Render(WorldState &rWorldState, std::vector<uint64> &rOverlappingObjects)
{
	auto start = std::chrono::high_resolution_clock::now();

	for (CObserver *pObserver : rWorldState.arrObservers)
	{
		for (CObject *pObject : rWorldState.arrObjects)
		{
			if (pObserver->GetBounds().Overlaps(pObject->GetPosition()))
			{
				rOverlappingObjects.push_back(pObject->GetIdentifier());
			}			
		}
	}

	return std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - start).count();
}


///////////////////////////////////////////////////////////////////////////////
int main()
{
	f64 initSummary = 0.0;
	f64 renderSummary = 0.0;
	f64 numberOfTests = 10.0;


	for(int j = 0; j < static_cast<int>(numberOfTests); j++)
	{
		WorldState state;
		initSummary += Init(state, SEED);
		for (size_t i = 0; i < 10; ++i)
		{
			std::vector<uint64> arrOverlappingObjects;

			const f64 fTimeUpdate = Update(state);
			const f64 fTimeRender = Render(state, arrOverlappingObjects);

			// std::cout << "Frame " << i << ". Update: " << fTimeUpdate << " ms. Render: " << fTimeRender << " ms. Visible Objects: " << arrOverlappingObjects.size() << " [";
			// for(uint64 nObject : arrOverlappingObjects)
			// 	std::cout << nObject << " ";
			// std::cout << "]" << std::endl;
			renderSummary += fTimeRender;
		}
	}

	std::cout << "Average init time: " << initSummary / numberOfTests << std::endl;
	std::cout << "Average render time: " << renderSummary / (10.0 * numberOfTests) << std::endl;

	// object tear-down/shutdown code can be omitted (in fact the OS will just clean that up for us more efficiently)
 	return EXIT_SUCCESS;
}

